// Copyright (c) 2012 Google Inc.
// See license at http://code.google.com/google_bsd_license.html

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "dart_api.h"
//#include "dart_debugger_api.h"

Dart_NativeFunction ResolveName(Dart_Handle name, int argc);


DART_EXPORT Dart_Handle ns2dart_extension_Init(Dart_Handle parent_library) {
  if (Dart_IsError(parent_library)) { return parent_library; }


  Dart_Handle result_code = Dart_SetNativeResolver(parent_library, ResolveName);
  if (Dart_IsError(result_code)) return result_code;

  return Dart_Null();
}

Dart_Handle HandleError(Dart_Handle handle) {
  if (Dart_IsError(handle)) Dart_PropagateError(handle);
  return handle;
}

void Instantiate0(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle name = Dart_GetNativeArgument(arguments, 0);
  Dart_Handle klass = Dart_GetClass(library, name);
  if(Dart_IsError(klass)) Dart_PropagateError(klass);

  Dart_Handle result = Dart_New(klass, Dart_Null(), 0, NULL);
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}

void Invoke(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  //Dart_Handle library = Dart_RootLibrary();
  Dart_Handle receiver = Dart_GetNativeArgument(arguments, 0);
  Dart_Handle selector = Dart_GetNativeArgument(arguments, 1);
  Dart_Handle argumentList = Dart_GetNativeArgument(arguments, 2);

  intptr_t arity = 0;
  Dart_Handle result = Dart_ListLength(argumentList, &arity);

  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_Handle args[arity];
  for(intptr_t i=0; i<arity; i++) args[i] = Dart_ListGetAt(argumentList, i);

  result = Dart_Invoke(receiver, selector, arity, args);
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}


void Instantiate1(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle name = Dart_GetNativeArgument(arguments, 0);
  Dart_Handle arg = Dart_GetNativeArgument(arguments, 1);
  Dart_Handle klass = Dart_GetClass(library, name);
  if(Dart_IsError(klass)) Dart_PropagateError(klass);

  Dart_Handle args[2];
  args[0] = arg;
  args[1] = 0;

	//printf("Before isClass=%d isError=%d\n", Dart_IsClass(klass), Dart_IsError(arg));
  Dart_Handle result = Dart_New(klass, Dart_Null(), 1, args);
	//printf("After\n");
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}


void SetStatic(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle klassname = Dart_GetNativeArgument(arguments, 0);
  Dart_Handle fieldname = Dart_GetNativeArgument(arguments, 1);
  Dart_Handle fieldvalue = Dart_GetNativeArgument(arguments, 2);

  Dart_Handle klass = Dart_GetClass(library, klassname);
  if(Dart_IsError(klass)) Dart_PropagateError(klass);

  Dart_Handle result = Dart_SetField(klass, fieldname, fieldvalue);
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}

void GetStatic(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle klassname = Dart_GetNativeArgument(arguments, 0);
  Dart_Handle fieldname = Dart_GetNativeArgument(arguments, 1);

  Dart_Handle klass = Dart_GetClass(library, klassname);
  if(Dart_IsError(klass)) Dart_PropagateError(klass);

  Dart_Handle result = Dart_GetField(klass, fieldname);
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}

void GetGlobal(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle fieldname = Dart_GetNativeArgument(arguments, 0);

  Dart_Handle result = Dart_GetField(library, fieldname);
  if(Dart_IsError(result)) Dart_PropagateError(result);

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}

void LoadSource(Dart_NativeArguments arguments){

  Dart_EnterScope();
  
  Dart_Handle library = Dart_RootLibrary();
  Dart_Handle url = //Dart_LibraryUrl(library);//
		Dart_NewStringFromCString("file:///tmp/dynamicallyloadedcode.dart");
  Dart_Handle source = Dart_GetNativeArgument(arguments, 0);

  if(Dart_IsError(library)) printf("Library is error");
  if(Dart_IsNull(library)) printf("Library is null");
  if(Dart_IsError(url)) printf("URL is error");
  if(Dart_IsNull(url)) printf("URL is null");
  if(Dart_IsError(source)) printf("source is error");
  if(Dart_IsNull(source)) printf("source is null");

  Dart_Handle result = Dart_LoadSource(library, url, source);

  if(Dart_IsError(result)) Dart_PropagateError(result);
	result = Dart_Null();

  Dart_SetReturnValue(arguments, result);

  Dart_ExitScope();
}


struct FunctionLookup {
  const char* name;
  Dart_NativeFunction function;
};

FunctionLookup function_list[] = {
    {"LoadSource", LoadSource},
    {"Instantiate0", Instantiate0},
    {"Instantiate1", Instantiate1},
    {"SetStatic", SetStatic},
    {"GetStatic", GetStatic},
    {"GetGlobal", GetGlobal},
    {"Invoke", Invoke},
    {NULL, NULL}};

Dart_NativeFunction ResolveName(Dart_Handle name, int argc) {
  if (!Dart_IsString(name)) return NULL;
  Dart_NativeFunction result = NULL;
  Dart_EnterScope();
  const char* cname;
  HandleError(Dart_StringToCString(name, &cname));

  for (int i=0; function_list[i].name != NULL; ++i) {
    if (strcmp(function_list[i].name, cname) == 0) {
      result = function_list[i].function;
      break;
    }
  }
  Dart_ExitScope();
  return result;
}
